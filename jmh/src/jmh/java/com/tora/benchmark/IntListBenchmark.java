package com.tora.benchmark;

import com.tora.repository.IntListRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 20, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class IntListBenchmark {

    @Param({"1", "31", "65", "101", "103", "1024", "10240", "65535", "21474836"})
    public int size;

    private IntListRepository repo;

    @Setup(Level.Trial)
    public void setup() {
        repo = new IntListRepository();
    }

    @Benchmark
    public void testCreate(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(i));
        consumer.consume(repo);
    }

    @Benchmark
    public void testContains(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(i));
        consumer.consume(repo.contains(size));
    }

    @Benchmark
    public void testRemove(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(i));
        Integer nrToRemove = size;
        repo.add(nrToRemove);
        consumer.consume(repo);
        repo.remove(nrToRemove);
    }

}