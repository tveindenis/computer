package com.tora.benchmark;

import com.tora.model.Order;
import com.tora.repository.HashSetRepository;
import com.tora.repository.InMemoryRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 20, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class HashSetBenchmark {

    @Param({"1", "31", "65", "101", "103", "1024", "10240", "65535", "21474836"})
    public int size;

    private InMemoryRepository<Order> repo;

    @Setup(Level.Trial)
    public void setup() {
        repo = new HashSetRepository<>();
    }

    @Benchmark
    public void testCreate(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(new Order(i, i, i)));
        consumer.consume(repo);
    }

    @Benchmark
    public void testContains(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(new Order(i, i, i)));
        consumer.consume(repo.contains(new Order(size, size, size)));
    }

    @Benchmark
    public void testRemove(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(new Order(i, i, i)));
        Order orderToRemove = new Order(size, size, size);
        repo.add(orderToRemove);
        consumer.consume(repo);
        repo.remove(orderToRemove);
    }
}

