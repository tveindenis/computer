package com.tora.repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetRepository<T> implements InMemoryRepository<T> {

    private final Set<T> hashSet;

    public HashSetRepository(){
        hashSet = new HashSet<>();
    }

    @Override
    public void add(T obj) {
        hashSet.add(obj);
    }

    @Override
    public boolean contains(T obj) {
        return hashSet.contains(obj);
    }

    @Override
    public void remove(T obj) {
        hashSet.remove(obj);
    }
}
