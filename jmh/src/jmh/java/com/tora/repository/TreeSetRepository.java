package com.tora.repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetRepository<T> implements InMemoryRepository<T> {

    private final Set<T> treeSet;

    public TreeSetRepository(){
        treeSet = new TreeSet<>();
    }

    @Override
    public void add(T t) {
        treeSet.add(t);
    }

    @Override
    public boolean contains(T t) {
        return treeSet.contains(t);
    }

    @Override
    public void remove(T t) {
        treeSet.remove(t);
    }
}
