package com.tora.repository;


import com.tora.model.Order;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepository<T extends Order> implements InMemoryRepository<T> {

    private final ConcurrentHashMap<Integer, T> concurrentHashMap;

    public ConcurrentHashMapRepository(){
        concurrentHashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T t) {
        concurrentHashMap.put(t.getId(), t);
    }

    @Override
    public boolean contains(T t) {
        return concurrentHashMap.contains(t);
    }

    @Override
    public void remove(T t) {
        concurrentHashMap.remove(t.getId());
    }
}
