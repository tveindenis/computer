package com.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Computer computer = new Computer();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Enter your choice: ");
            menu();
            int input = scanner.nextInt();

            if(input < 0 || input > 4){
                System.out.println("Invalid choice");
                continue;
            }else if(input == 0){
                scanner.close();
                return;
            }

            try {
                double a;
                double b;
                double result = 0;
                System.out.println("Enter the first number: ");
                a = scanner.nextInt();
                System.out.println("Enter the second number: ");
                b = scanner.nextInt();

                switch (input){
                    case 1:
                        result = computer.add(a, b);
                        break;
                    case 2:
                        result = computer.subtract(a, b);
                        break;
                    case 3:
                        result = computer.multiply(a, b);
                        break;
                    case 4:
                        result = computer.divide(a, b);
                        break;
                    default: break;
                }

                System.out.println("Result: " + result);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void menu(){
        System.out.println("0. Exit");
        System.out.println("1. Add");
        System.out.println("2. Subtract");
        System.out.println("3. Multiply");
        System.out.println("4. Divide");
    }
}
