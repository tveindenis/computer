package com.tora.repository;

import it.unimi.dsi.fastutil.ints.IntArrayList;

public class IntListRepository implements InMemoryRepository<Integer> {

    private final IntArrayList intList;

    public IntListRepository() {
        this.intList = new IntArrayList();
    }

    @Override
    public void add(Integer i) {
        this.intList.add(i.intValue());
    }

    @Override
    public boolean contains(Integer i) {
        return this.intList.contains(i.intValue());
    }

    @Override
    public void remove(Integer i) {
        this.intList.removeInt(i);
    }
}
