package com.tora.repository;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

public class MutableListRepository<T> implements InMemoryRepository<T> {

    private final MutableList<T> mutableList;

    public MutableListRepository(){
        this.mutableList = FastList.newList();
    }

    @Override
    public void add(T t) {
        this.mutableList.add(t);
    }

    @Override
    public boolean contains(T t) {
        return this.mutableList.contains(t);
    }

    @Override
    public void remove(T t) {
        this.mutableList.remove(t);
    }
}

