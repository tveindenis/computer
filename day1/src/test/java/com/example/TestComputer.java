package com.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestComputer {

    public Computer computer;

    @BeforeEach
    public void setup(){
        computer = new Computer();
    }

    @Test
    public void computerShouldAddCorrectly(){
        double a1 = 2;
        double b1 = 3;
        double a2 = 2;
        double b2 = -3;
        double a3 = -2;
        double b3 = 3;
        double a4 = 0;
        double b4 = 0;
        assertEquals(5.0, computer.add(a1, b1));
        assertEquals(-1.0, computer.add(a2, b2));
        assertEquals(1.0, computer.add(a3, b3));
        assertEquals(0.0, computer.add(a4, b4));
    }

    @Test
    public void computerShouldSubtractCorrectly(){
        double a1 = 2;
        double b1 = 3;
        double a2 = 2;
        double b2 = -3;
        double a3 = -2;
        double b3 = 3;
        double a4 = 0;
        double b4 = 0;
        assertEquals(-1.0, computer.subtract(a1, b1));
        assertEquals(5.0, computer.subtract(a2, b2));
        assertEquals(-5.0, computer.subtract(a3, b3));
        assertEquals(0.0, computer.subtract(a4, b4));
    }

    @Test
    public void computerShouldMultiplyCorrectly(){
        double a1 = 2;
        double b1 = 3;
        double a2 = 2;
        double b2 = -3;
        double a3 = -2;
        double b3 = 3;
        double a4 = 0;
        double b4 = 0;
        double a5 = 0;
        double b5 = 3;
        double a6 = 3;
        double b6 = 0;
        assertEquals(6.0, computer.multiply(a1, b1));
        assertEquals(-6.0, computer.multiply(a2, b2));
        assertEquals(-6.0, computer.multiply(a3, b3));
        assertEquals(0.0, computer.multiply(a4, b4));
        assertEquals(0.0, computer.multiply(a5, b5));
        assertEquals(0.0, computer.multiply(a6, b6));
    }

    @Test
    public void computerShouldDivideCorrectlyWhenDenominatorIsNotZero(){
        double a1 = 2;
        double b1 = 4;
        double a2 = 6;
        double b2 = -3;
        double a3 = -2;
        double b3 = 2;
        assertEquals(0.5, computer.divide(a1, b1));
        assertEquals(-2.0, computer.divide(a2, b2));
        assertEquals(-1.0, computer.divide(a3, b3));
    }

    @Test
    public void computerShouldThrowExceptionWhenDenominatorIsZero(){
        double a1 = 2;
        double b1 = 0;
        double a2 = -6;
        double b2 = 0;
        double a3 = 0;
        double b3 = 0;
        assertThrows(IllegalArgumentException.class,
                () -> computer.divide(a1, b1));
        assertThrows(IllegalArgumentException.class,
                () -> computer.divide(a2, b2));
        assertThrows(IllegalArgumentException.class,
                () -> computer.divide(a3, b3));
    }
}
